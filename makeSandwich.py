
CheseeAmount=5
TomatoAmount=3
CheddarAmount=1


def AskSandwichType():
    print("\n")
    print("Please Select sandwhich type:")
    print("1. chesee")
    print("2. cheddar")
    print("3. tomato")
    print("4. mix")
    sandwichNo=input("which number do you prefer?")
    return sandwichNo


def AskAgain(sandwichType):
    print("\n")
    print("\n")
    print("###########################")
    print("There is no "+sandwichType )
    print("Please select another one")
    print("###########################")
    main()


def PrepareSandwichType(sandwichType):
    if(sandwichType== "1"):
        PrepareCheseeSandwich()
    elif(sandwichType== "2"):
        PrepareCheddarSandwich()
    elif(sandwichType== "3"):
        PrepareTomatoSandwich()
    elif(sandwichType== "4"):
        PrepareMixSandwich()
    main()


def PrepareCheseeSandwich():
    global CheseeAmount
    if(CheseeAmount>0):
        CheseeAmount-=1
        print(CheseeAmount)
        print(" ********* ")
        print("***********")
        print("-----------")
        print("-----------")
        print("***********")
        print(" ********* ")
    else:
        AskAgain("Chesee")


def PrepareCheddarSandwich():
    global CheddarAmount
    if(CheddarAmount>0):
        CheddarAmount-=1
        print(" ********* ")
        print("***********")
        print("--0--0---0-")
        print("---0---0---")
        print("***********")
        print(" ********* ")
    else:
        AskAgain("Cheddar")


def PrepareTomatoSandwich():
    global TomatoAmount
    if(TomatoAmount>0):
        TomatoAmount-=1
        print(" ********* ")
        print("***********")
        print("---%--%--%--")
        print("--%--%---%--")
        print("***********")
        print(" ********* ")
    else:
        AskAgain("Tomato")


def PrepareMixSandwich():

    global CheseeAmount
    global TomatoAmount
    global CheddarAmount

    if(CheseeAmount>0 and CheddarAmount>0 and TomatoAmount>0):

        CheseeAmount-=1
        CheddarAmount-=1
        TomatoAmount-=1

        print(" ********* ")
        print("***********")
        print("-0-%-0-%0%--")
        print("-%-0-%-0-%--")
        print("***********")
        print(" ********* ")
    else:
        AskAgain("Mix")



def main():

    print("Welcome To Real SandWhich Places")
    print("Please select one sandwich type:")
    sandwichTpe=AskSandwichType()
    PrepareSandwichType(sandwichTpe)

main()
